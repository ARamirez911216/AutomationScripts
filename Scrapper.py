from lxml import html
import requests, sys, argparse, os
import polib, subprocess, re

#Translation functions by Rory McCann <rory@technomancy.org>
__author__ = ""

def translate_subpart(string, lang_direction):
    """Simple translate for just a certin string"""

    for codes in lang_direction.split("/"):
        translater = subprocess.Popen(['apertium', '-u', '-f', 'html', codes], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        translater.stdin.write(string.encode("utf-8")+"\n".encode("utf-8"))
        string, _ = translater.communicate()
        string = string[:-1].decode("utf-8")

    return string

def translate(string, lang_direction):
    """Takes a string that is to be translated and returns the translated string, doesn't translate the %(format)s parts, they must remain the same text as the msgid"""
    # simple format chars like %s can be 'translated' ok, they just pass through unaffected
    named_format_regex = re.compile(r"%\([^\)]+?\)[sd]", re.VERBOSE)
    matches = named_format_regex.findall(string)
    new = None

    if len(matches) == 0:
        # There are no format specifiers in this string, so just do a straight translation

        # this fails if we've missed a format specifier
        assert "%(" not in string, string

        new = translate_subpart(string, lang_direction)

    else:

        # we need to do complicate translation of the bits inside
        full_trans = translate_subpart(string, lang_direction)

        for match in matches:
            # then, for each format specifier, replace back in the string

            translated_match = translate_subpart(match, lang_direction)

            # during the translation some extra punctuation/spaces might have been added
            # remove them
            translated_match_match = named_format_regex.search(translated_match)
            assert translated_match_match
            translated_match = translated_match_match.group(0)

            # put back the format specifier, the case of the format specifier might have changed
            replace = re.compile(re.escape(translated_match), re.IGNORECASE)
            full_trans = replace.sub(match, full_trans)

        
        new = full_trans

    return new

if __name__ == '__main__':
	directory = "fanfics/"
	filename = "original.txt"
	path = directory + filename
	#Takes the command line argument.
	parser = argparse.ArgumentParser()
	parser.add_argument("webadress", help="Adress of the\
	web page to scrap.")
	args = parser.parse_args()

	#Retrieve web page data.
	page = requests.get(args.webadress)
	tree = html.fromstring(page.content)

	#Based on id.
	#for td in tree.xpath("//div[@id='storytextp']"):

	#Find all paragraphs.
	data = []
	i = 0
	for td in tree.xpath("//article"):
		#Print content.
		data.append(td.text_content())
		print(data[i])
		i = i+1

	#Check if directory exists, otherwise create it.
	if not os.path.exists(directory):
		#Create directory.
		os.makedirs(directory)
	#Create fanfic file.
	with open(path, "wb") as file_:
		#Dump all data.
		for i in range(0, len(data)):
			file_.write(data[i])
			file_.write("\n\n")
		file_.close()

	#Translate.
	filename = "translated.txt"
	path = directory + filename
	with open(path, "wb") as file_:
		#Store all data on list.
		for i in range(0, len(data)):
			translated = translate(data[i], "en-es").replace("&a", "á").replace("&e", "é").replace("&i", "í").replace("&o", "ó").replace("&u", "ú").replace("&ntilde", "ñ").replace("acute", "").replace(";", "")
			file_.write(translated)
			file_.write("\n\n")
			print("Iteration " + str(i))
		file_.close()