#!/bin/bash
#This application starts the main applications I make
#use of in my day.
#Author: Angel Arturo Ramirez Suarez

user="Angel Arturo"

#Initialize Web Browser
if [ $1==START ]
	then
		#Initialize Python start my day script
		#This will be hell to maintain, need to update selenium or seek
		#a better alternative
		python3.5 /home/arthur/Programs/Automation_scripts/startmyday.py
	else
		#Initialize standard browser
		google-chrome &
fi
#Initialize caja file manager
caja &
#Initialize thunderbird
thunderbird &
#Initialize anki
anki &
#Initialize tomate Pomodoro application
tomate &
#Initialize spotify music application
spotify &
#Message to indicate we're done.
echo Have a productive day $user!